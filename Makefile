.PHONY: setup

setup:
	stack setup

.PHONY: build

build:
	stack build

.PHONY: repl

repl:
	stack ghci

.PHONY: run

run: build
	stack exec app

.PHONY: test
test:
	stack test -j11

.PHONY: clean

clean:
	stack clean

