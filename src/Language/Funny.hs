module Language.Funny
  ( module Export
  , runExpr
  )
where

import Language.Funny.Ast as Export
import Language.Funny.Utils as Export
import Language.Funny.Infer as Export
import Language.Funny.Interpret as Export

import Control.Monad.Reader

runExpr :: Expr -> Either Err Atom
runExpr =
  infer builtinFunTypes
    >=> checkFreeVars builtinFunTypes
    >=> pure . removeTypes
    >=> flip runReaderT builtinFuns . eval
