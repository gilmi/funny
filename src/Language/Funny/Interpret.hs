{-# LANGUAGE ConstraintKinds, FlexibleContexts #-}

module Language.Funny.Interpret where

import Control.Monad.Except
import Control.Monad.Reader

import Language.Funny.Ast
import Language.Funny.Utils

----------
-- Eval --
----------

type Interpret m = (MonadError Err m, MonadReader (Env Atom) m)

eval :: Interpret m => Expr -> m Atom
eval expression =
  case expression of
    EAtom _ atom ->
      pure atom

    EVar _ var ->
      lookupVar var

    EFun ann var body -> do
      env <- ask
      pure $ Closure env (EFun ann var body)

    ELet _ var binding body -> do
      binding' <- eval binding
      local (insertToEnv var binding') $ eval body

    EApp _ fun arg -> do
      fun' <- eval fun
      case fun' of
        Closure closureEnv (EFun _ var body) -> do
          arg' <- eval arg
          local (const $ insertToEnv var arg' closureEnv) $
            eval body

        Builtin _ builtin -> do
          liftEither . builtin =<< eval arg

        other -> typecheckFailedFun other

-- Errors

typecheckFailedFun :: MonadError Err m => Atom -> m a
typecheckFailedFun expr = throwError $ unlines
  [ "Unexpected error: typecheck failed asserting the this expression:"
  , show expr
  , "should have be a function."
  ]

