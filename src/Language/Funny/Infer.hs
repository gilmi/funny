{-# LANGUAGE DeriveFoldable, DeriveFoldable, DeriveTraversable #-}
{-# LANGUAGE ScopedTypeVariables, ViewPatterns #-}
{-# LANGUAGE ConstraintKinds, FlexibleContexts #-}

module Language.Funny.Infer where

import Data.List
import Control.Monad.Except
import Control.Monad.State
import Control.Monad.Reader
import qualified Data.Map as M
import qualified Data.Set as S

import Language.Funny.Ast
import Language.Funny.Utils


--------------------
-- Type Inference --
--------------------

-- Types --

type Constraint = (Type, Type)
type Constraints = S.Set Constraint
type Substitutions = M.Map String Type

data InferState
  = InferState
  { freshTVars :: [String]
  , constraints :: Constraints
  }

type InferReader = M.Map String Type

type Infer m =
  ( MonadState InferState m
  , MonadReader InferReader m
  , MonadError Err m
  )

-- API --


infer :: MonadError Err m => Env Type -> Expr -> m (TypedExpr)
infer env expr = do
  (expr', constraints') <- assignCollect env expr
  subs <- unifyAll constraints'
  pure $ applySubstitutions subs expr'



-- Assign types and collect constraints --

assignCollect :: MonadError Err m => Env Type -> Expr -> m (TypedExpr, Constraints)
assignCollect env =
  ( fmap (fmap constraints)
  . flip runStateT (InferState freshTVarsList mempty)
  . flip runReaderT env
  . assignCollectImpl
  )

assignCollectImpl :: Infer m => Expr -> m (TypedExpr)
assignCollectImpl expression =
  case expression of
    EAtom _ atom ->
      case atom of
        Lit lit ->
          pure $ case lit of
            LInt{} -> EAtom tInt atom
            LBool{} -> EAtom tBool atom
            LString{} -> EAtom tString atom
        _ -> unexpectedAtom atom

    EVar _ var ->
      EVar <$> assignTVar var <*> pure var

    EFun _ var body -> do
      vartn <- freshTVarName
      let
        vart = TVar vartn
      bodyt <- local (insertToEnv var vart) $ assignCollectImpl body
      pure $ EFun (Forall [vartn] $ TFun vart (getType bodyt)) var bodyt

    ELet _ var binding body -> do
      bindingt <- assignCollectImpl binding
      bodyt <- local (insertToEnv var $ getType bindingt) $ assignCollectImpl body
      pure $ ELet (getType bodyt) var bindingt bodyt

    EApp _ fun arg -> do
      funt <- assignCollectImpl fun
      argt <- assignCollectImpl arg
      result <- freshTVar
      funt' <- tryReplaceQuantified $ getType funt
      addConstraint funt' (TFun (getType argt) result)
      pure $ EApp result funt argt

freshTVarsList :: [String]
freshTVarsList = concat
  [ map pure ['a'..'z']
  , [ ("t" <> show i) | i <- [1..] :: [Int] ]
  ]

freshTVar :: MonadState InferState m => m Type
freshTVar = TVar <$> freshTVarName

freshTVarName :: MonadState InferState m => m String
freshTVarName = do
  name <- head . freshTVars <$> get
  modify $ \s -> s { freshTVars = tail (freshTVars s) }
  pure name

assignTVar
  :: MonadState InferState m
  => MonadReader InferReader m
  => String -> m Type
assignTVar = maybeAssignTVar freshTVar

maybeAssignTVar
  :: MonadState InferState m
  => MonadReader InferReader m
  => m Type -> String -> m Type
maybeAssignTVar genTVar name = do
  r <- asks (M.lookup name)
  case r of
    Nothing -> genTVar
    Just typ -> tryReplaceQuantified typ

tryReplaceQuantified
  :: MonadState InferState m
  => MonadReader InferReader m
  => Type -> m Type
tryReplaceQuantified typ =
  case typ of
    Forall quantified typ' -> do
      quantified' <- M.fromList <$> traverse (\qname -> (,) qname . Forall [] <$> freshTVar) quantified
      typ'' <- tryReplaceQuantified typ'
      runReaderT (replaceQuantified typ'') quantified'
    TFun arg body ->
      TFun
        <$> tryReplaceQuantified arg
        <*> tryReplaceQuantified body
    _ -> pure typ

replaceQuantified
  :: MonadState InferState m
  => MonadReader InferReader m
  => Type -> m Type
replaceQuantified typ =
  case typ of
    TVar var ->
      maybeAssignTVar (pure $ TVar var) var
    TFun arg body ->
      TFun
        <$> replaceQuantified arg
        <*> replaceQuantified body
    TCon con -> pure (TCon con)
    Forall xs t ->
      local (flip (foldr M.delete) xs) $ replaceQuantified t


addConstraint :: Infer m => Type -> Type -> m ()
addConstraint t1 t2 =
  modify $ \s -> s { constraints = S.insert (t1, t2) (constraints s) }

-- Unification --


unifyAll :: MonadError Err m => Constraints -> m Substitutions
unifyAll constraints' =
  foldM unify mempty (S.toList constraints')

-- See http://www.norvig.com/unify-bug.pdf
unify :: MonadError Err m => Substitutions -> Constraint -> m Substitutions
unify substitutions constraint = case constraint of
  (TVar var1, t2) ->
    unifyTVar substitutions var1 t2

  (t1, TVar var2) ->
    unifyTVar substitutions var2 t1

  (TFun arg1 body1, TFun arg2 body2) ->
    flip unify (arg1, arg2) =<< unify substitutions (body1, body2)

  (t1, t2)
    | t1 == t2 -> pure substitutions
    | otherwise -> typesDoNotUnify t1 t2

unifyTVar :: MonadError Err m => Substitutions -> String -> Type -> m Substitutions
unifyTVar substitutions var1 t2
  | TVar var1 == t2 =
      pure substitutions

  | Just t1 <- M.lookup var1 substitutions =
      unify substitutions (t1, t2)

  | TVar var2 <- t2 =
    case M.lookup var2 substitutions of
      Just x -> unify substitutions (TVar var1, x)
      Nothing -> pure $ insertToEnv var1 t2 substitutions

  | occursCheck substitutions var1 t2 =
    occursCheckFail var1 t2

  | otherwise = pure $ insertToEnv var1 t2 substitutions

occursCheck :: Substitutions -> String -> Type -> Bool
occursCheck substitutions var1 t2 =
  case t2 of
    TVar var2
      | var1 == var2 -> True
      | Just t2' <- M.lookup var2 substitutions ->
        occursCheck substitutions var1 t2'
    TFun arg ret ->
      (||)
        (occursCheck substitutions var1 arg)
        (occursCheck substitutions var1 ret)
    _ ->
      False

-- Apply --

applySubstitutions :: Substitutions -> TypedExpr -> TypedExpr
applySubstitutions substitutions expr =
  fmap applySubstType expr
  where
    applySubstType currentType =
      case currentType of
        TVar var -> maybe (TVar var) applySubstType $ M.lookup var substitutions
        TFun arg ret -> TFun (applySubstType arg) (applySubstType ret)
        t -> t

-- Errors

checkFreeVars :: MonadError Err m => Env a -> ExprA b -> m (ExprA b)
checkFreeVars env expr =
  let
    freevars = exprFreeVars (M.keysSet env) expr
  in
    if S.null freevars
      then pure expr
      else
      throwError $ "Found a free variable not in the environment: " <> (intercalate ", " (S.toList freevars))

exprFreeVars :: S.Set String -> ExprA b -> S.Set String
exprFreeVars env expr = case expr of
  EAtom{} -> mempty
  EVar _ var
    | S.member var env -> mempty
    | otherwise -> S.singleton var
  EFun _ arg body ->
    exprFreeVars (S.insert arg env) body
  ELet _ name binding body ->
    (<>)
      (exprFreeVars env binding)
      (exprFreeVars (S.insert name env) body)
  EApp _ fun arg ->
    (<>)
      (exprFreeVars env fun)
      (exprFreeVars env arg)


typesDoNotUnify :: MonadError Err m => Type -> Type -> m any
typesDoNotUnify t1 t2 =
  throwError $ "Types do not unify: " <> show t1 <> ", " <> show t2


occursCheckFail :: MonadError Err m => String -> Type -> m any
occursCheckFail var1 t2 =
  throwError $ "Occurs check failed for: " <> show var1 <> " in " <> show t2

unexpectedAtom :: MonadError Err m => Atom -> m any
unexpectedAtom atom = throwError $ "Unexpected atom: " <> show atom
