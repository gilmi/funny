{-# LANGUAGE FlexibleContexts #-}

module Language.Funny.Utils where

import Control.Monad.Except
import Control.Monad.Reader
import qualified Data.Map as M
import Text.Groom
import Debug.Trace

import Language.Funny.Ast

insertToEnv :: String -> a -> Env a -> Env a
insertToEnv = M.insert

errNotAFunction :: MonadError Err m => Atom -> m any
errNotAFunction atom =
  throwError $ "Not a function: " <> show atom

lookupVar :: MonadReader (Env a) m => MonadError Err m => Show a => String -> m a
lookupVar var = do
  env <- ask
  maybe
    (throwError $ "Could not find var '" <> var <> "' in environment " <> show env)
    pure
    (M.lookup var env)


getType :: TypedExpr -> Type
getType expression =
  case expression of
    EAtom ann _ -> ann
    EVar ann _ -> ann
    EFun ann _ _ -> ann
    ELet ann _ _ _ -> ann
    EApp ann _ _ -> ann

updateType :: (Type -> Type) -> TypedExpr -> TypedExpr
updateType f expression =
  case expression of
    EAtom typ a -> EAtom (f typ) a
    EVar typ var -> EVar (f typ) var
    EFun typ arg body -> EFun (f typ) arg body
    ELet typ var binding body -> ELet (f typ) var binding body
    EApp typ e1 e2 -> EApp (f typ) e1 e2

removeTypes :: TypedExpr -> Expr
removeTypes = fmap (const ())

ltrace :: Show a => String -> a -> a
ltrace label a = trace (label <> ": " <> groom a) a
{-# WARNING ltrace "ltrace left in the code" #-}
