{-# LANGUAGE DeriveFoldable, DeriveFoldable, DeriveTraversable #-}
{-# LANGUAGE ConstraintKinds, FlexibleContexts #-}

module Language.Funny.Ast where

import Control.Monad.Except
import qualified Data.Map as M

----------
-- Expr --
----------

type Expr = ExprA ()


data ExprA ann
  = EAtom ann Atom
  | EVar ann String
  | EFun ann String (ExprA ann)
  | ELet ann String (ExprA ann) (ExprA ann)
  | EApp ann (ExprA ann) (ExprA ann)
  deriving (Show, Functor, Foldable, Traversable)

data Atom
  = Lit Lit
  | Closure (Env Atom) Expr
  | Builtin String (Atom -> Either Err Atom)

data Lit
  = LInt Int
  | LBool Bool
  | LString String
  deriving (Show, Eq)

type Env a = M.Map String a

instance Show Atom where
  show atom = case atom of
    Lit lit -> "(Lit (" <> show lit <> "))"
    Closure env expr -> "(Closure " <> show env <> ": " <> show expr <> ")"
    Builtin name _ -> "Builtin<" <> name <> ">"

instance Eq Atom where
  atom1 == atom2 = case (atom1, atom2) of
    (Lit lit1, Lit lit2) -> lit1 == lit2
    _ -> False

type Err = String

-----------
-- Types --
-----------

type TypedExpr = ExprA Type

data Type
  = TCon String
  | TVar String
  | TFun Type Type
  | Forall [String] Type
  deriving (Show, Eq, Ord)

tInt :: Type
tInt = TCon "Int"

tBool :: Type
tBool = TCon "Bool"

tString :: Type
tString = TCon "String"


----------
-- EDSL --
----------

var_ :: String -> Expr
var_ = EVar ()

int_ :: Int -> Expr
int_ = EAtom () . Lit . LInt

str_ :: String -> Expr
str_ = EAtom () . Lit . LString

false_ :: Expr
false_ = EAtom () . Lit . LBool $ False

true_ :: Expr
true_ = EAtom () . Lit . LBool $ True

fun_ :: String -> Expr -> Expr
fun_ = EFun ()

let_ :: String -> Expr -> Expr -> Expr
let_ = ELet ()

app_ :: Expr -> Expr -> Expr
app_ = EApp ()

not_ :: Expr -> Expr
not_ = app_ (var_ "not")

equals_ :: Expr -> Expr -> Expr
equals_ e1 e2 = app_ (app_ (var_ "equals") e1) e2

add_ :: Expr -> Expr -> Expr
add_ e1 e2 = app_ (app_ (var_ "add") e1) e2

sub_ :: Expr -> Expr -> Expr
sub_ e1 e2 = app_ (app_ (var_ "sub") e1) e2

mul_ :: Expr -> Expr -> Expr
mul_ e1 e2 = app_ (app_ (var_ "mul") e1) e2

div_ :: Expr -> Expr -> Expr
div_ e1 e2 = app_ (app_ (var_ "div") e1) e2


concat_ :: Expr -> Expr -> Expr
concat_ e1 e2 = app_ (app_ (var_ "concat") e1) e2

take_ :: Expr -> Expr -> Expr
take_ e1 e2 = app_ (app_ (var_ "take") e1) e2

drop_ :: Expr -> Expr -> Expr
drop_ e1 e2 = app_ (app_ (var_ "drop") e1) e2

-- fun lambdas

id_ :: Expr
id_ = fun_ "x" (var_ "x")

const_ :: Expr
const_ = fun_ "a" (fun_ "b" (var_ "a"))

-- Builtins

builtinFuns :: Env Atom
builtinFuns = M.fromList $ fmap (\(name, _, body) -> (name, body)) builtins

builtinFunTypes :: Env Type
builtinFunTypes = M.fromList $ fmap (\(name, typ, _) -> (name, typ)) builtins

builtins :: [(String, Type, Atom)]
builtins =
  [ binIntOp "add" (pure .: (+))
  , binIntOp "sub" (pure .: (-))
  , binIntOp "mul" (pure .: (*))
  , binIntOp "div" (\x y -> if y /= x then pure (x `div` y) else throwError "Division by zero")
  , ( "not"
    , Forall [] $ TFun tBool tBool
    , Builtin "not" $ \arg1 ->
      case arg1 of
        Lit (LBool bool) ->
          pure $ Lit $ LBool (not bool)
        other -> typecheckFailed tBool other
    )
  , ( "equals"
    , Forall ["a"] $ TFun (TVar "a") (TFun (TVar "a") tBool)
    , Builtin "equals" $ \arg1 ->
      case arg1 of
        Lit lit1 ->
          pure $ Builtin "#equals" $ \arg2 ->
            case arg2 of
              Lit lit2 ->
                pure $ Lit $ LBool (lit1 == lit2)
              _ ->
                pure $ Lit $ LBool False
        _ ->
          pure $ Builtin "#equals" $ \arg2 ->
            case arg2 of
              _ ->
                pure $ Lit $ LBool False
    )
  , ( "concat"
    , Forall [] $ TFun tString (TFun tString tString)
    , Builtin "concat" $ \arg1 ->
      case arg1 of
        Lit (LString str1) ->
          pure $ Builtin "#concat" $ \arg2 ->
            case arg2 of
              Lit (LString str2) ->
                pure $ Lit $ LString (str1 <> str2)
              other -> typecheckFailed tString other
        other -> typecheckFailed tString other
    )
  , ( "take"
    , Forall [] $ TFun tInt (TFun tString tString)
    , Builtin "take" $ \arg1 ->
      case arg1 of
        Lit (LInt int1) ->
          pure $ Builtin "#take" $ \arg2 ->
            case arg2 of
              Lit (LString str2) ->
                pure $ Lit $ LString (take int1 str2)
              other -> typecheckFailed tString other
        other -> typecheckFailed tInt other
    )
  , ( "drop"
    , Forall [] $ TFun tInt (TFun tString tString)
    , Builtin "drop" $ \arg1 ->
      case arg1 of
        Lit (LInt int1) ->
          pure $ Builtin "#drop" $ \arg2 ->
            case arg2 of
              Lit (LString str2) ->
                pure $ Lit $ LString (drop int1 str2)
              other -> typecheckFailed tString other
        other -> typecheckFailed tInt other
    )
  ]

(.:) :: (c -> d) -> (a -> b -> c) -> (a -> b -> d)
f .: g = \x y -> f (g x y)

binIntOp :: String -> (Int -> Int -> Either Err Int) -> (String, Type, Atom)
binIntOp name op =
  ( name
  , Forall [] $ TFun tInt (TFun tInt tInt)
  , Builtin name $ \arg1 ->
    case arg1 of
      Lit (LInt int1) ->
        pure $ Builtin ("#" <> name) $ \arg2 ->
          case arg2 of
            Lit (LInt int2) ->
              Lit . LInt <$> op int1 int2
            other -> typecheckFailed tInt other
      other -> typecheckFailed tInt other
  )

-- Errors

typecheckFailed :: Type -> Atom -> Either String a
typecheckFailed typ expr = Left $ unlines
  [ "Unexpected error: typecheck failed asserting the this expression:"
  , show expr
  , "should have the type: "
  , show typ
  ]
