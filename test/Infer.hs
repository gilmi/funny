
module Infer where

import Test.Hspec

import Data.Foldable
import qualified Data.Map as M

import Language.Funny.Ast
import Language.Funny.Infer

tests :: Spec
tests = do
  describe "Infer" $ do
    describe "Positive" testInferPos
    describe "Negative" testInferNeg


testInferPos :: Spec
testInferPos = do
  it "Lit bool" $ do
    shouldBe
      (topType <$> infer mempty false_)
      (Right tBool)

  it "Lit int" $ do
    shouldBe
      (topType <$> infer mempty (int_ 17))
      (Right tInt)

  it "Var" $ do
    shouldBe
      (topType <$> infer (M.fromList [("x", Forall [] tBool)]) (var_ "x"))
      (Right tBool)

  it "Not" $ do
    shouldBe
      (topType <$> infer builtinFunTypes (not_ false_))
      (Right tBool)

  it "Add" $ do
    shouldBe
      (topType <$> infer builtinFunTypes (add_ (int_ 1) (int_ 2)))
      (Right tInt)

  it "Id Bool" $ do
    shouldBe
      (topType <$> infer builtinFunTypes (app_ id_ false_))
      (Right tBool)

  it "Id Int" $ do
    shouldBe
      (topType <$> infer builtinFunTypes (app_ id_ (int_ 1)))
      (Right tInt)

  it "Equals Bool" $ do
    shouldBe
      (topType <$> infer builtinFunTypes (equals_ true_ false_))
      (Right tBool)

  it "Equals Int" $ do
    shouldBe
      (topType <$> infer builtinFunTypes (equals_ (int_ 1) (int_ 2)))
      (Right tBool)

  it "Equals Equals Int Bool" $ do
    shouldBe
      (topType <$> infer builtinFunTypes (equals_ (equals_ (int_ 1) (int_ 2)) false_))
      (Right tBool)

  it "Equals var Int" $ do
    shouldBe
      (topType <$> infer builtinFunTypes (equals_ (var_ "x") (int_ 2)))
      (Right tBool)

  it "Equals var Bool" $ do
    shouldBe
      (topType <$> infer builtinFunTypes (equals_ (var_ "x") false_))
      (Right tBool)

  it "Add var Int" $ do
    shouldBe
      (topType <$> infer builtinFunTypes (add_ (var_ "x") (int_ 2)))
      (Right tInt)

  it "Complex result" $ do
    shouldBe
      ( fmap topType $ infer builtinFunTypes $
        equals_
          (add_ (app_ id_ (var_ "x")) (var_ "y"))
          (sub_ (var_ "x") (var_ "y"))
      )
      (Right tBool)

  it "Let polymorphism" $ do
    shouldBe
      ( fmap topType $ infer builtinFunTypes $
        let_ "id" id_
          ( equals_
            (equals_ (app_ (var_ "id") (int_ 1)) (int_ 1))
            (app_ (var_ "id") true_)
          )
      )
      (Right tBool)

  it "Let polymorphism 2" $ do
    shouldBe
      ( fmap topType $ infer builtinFunTypes $
        let_ "selfquals" (fun_ "x" (equals_ (var_ "x") (var_ "x")))
          ( equals_
            (app_ (var_ "selfquals") (int_ 1))
            (app_ (var_ "selfquals") false_)
          )
      )
      (Right tBool)

  it "Let polymorphism const" $ do
    shouldBe
      ( fmap topType $ infer builtinFunTypes $
        let_ "const" const_
          ( equals_
            (app_ (app_ (var_ "const") (int_ 1)) (int_ 1))
            (app_ (app_ (var_ "const") (int_ 1)) false_)
          )
      )
      (Right tBool)

testInferNeg :: Spec
testInferNeg = do
  it "Not" $ do
    shouldBe
      (topType <$> infer builtinFunTypes (not_ (int_ 1)))
      (typesDoNotUnify tBool tInt :: Either Err Type)

{-
  it "Arity 1 0" $ do
    shouldBe
      (infer mempty $ app_ "not" [])
      (Left $ ArityMismatch "not" 1 0)

  it "Arity 1 2" $ do
    shouldBe
      (infer mempty $ app_ "not" [false_, false_])
      (Left $ ArityMismatch "not" 1 2)

  it "Add first" $ do
    shouldBe
      (infer mempty $ app_ "add" [false_, int_ 2])
      (Left $ TypesDoNotUnify TInt32 TBool)

  it "Add second" $ do
    shouldBe
      (infer mempty $ app_ "add" [int_ 2, false_])
      (Left $ TypesDoNotUnify TInt32 TBool)

  it "Equals bool" $ do
    shouldBe
      (infer mempty $ app_ "equals" [true_, int_ 1])
      (Left $ TypesDoNotUnify TBool TInt32)

  it "Equals int" $ do
    shouldBe
      (infer mempty $ app_ "equals" [int_ 1, false_])
      (Left $ TypesDoNotUnify TBool TInt32)

  it "Var do not unify" $ do
    shouldBe
      (infer mempty $ app_ "equals" [app_ "not" [var_ "x"], app_ "add" [var_ "x", int_ 1]])
      (Left $ TypesDoNotUnify TBool TInt32)
-}

topType :: ExprA Type -> Type
topType = head . toList
