
module Run where

import Test.Hspec


import Language.Funny.Ast
import Language.Funny

tests :: Spec
tests = do
  describe "Interpret" $ do
    it "eat" $ do
      shouldBe
        (runExpr eat)
        (Right $ Lit (LString "eat"))



eat :: Expr
eat =
  take_
    (int_ 3)
    ( drop_
      ( add_
        ( add_
          (int_ $ length "Can't")
          (int_ $ length "spell")
        )
        (int_ 5)
      )
      (str_ "Can't spell 'great' without...")
    )
