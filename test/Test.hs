{-# LANGUAGE NumericUnderscores #-}

import Test.Hspec
import System.Timeout

import qualified Infer as Infer
import qualified Run as Run

main :: IO ()
main = do
  timeout (20_000_000) tests
    >>= maybe (error "suite timeout") (const $ pure ())

tests :: IO ()
tests =
  hspec $ do
    Infer.tests
    Run.tests
