module Main where

import Language.Funny
import System.Exit

main :: IO ()
main = do
  case runExpr expr of
    Right res ->
      print res
    Left err -> do
      print err
      exitFailure

expr :: Expr
expr =
  take_
    (int_ 3)
    ( drop_
      ( add_
        ( add_
          (int_ $ length "Can't")
          (int_ $ length "spell")
        )
        (int_ 5)
      )
      (str_ "Can't spell 'great' without...")
    )
